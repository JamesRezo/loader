# spip_loader

[![spip.net](logo-spip.jpg)](https://spip.net/)

«spip_loader» est un script qui permet d'installer ou de mettre à jour votre site SPIP automatiquement
sans avoir à transférer l'intégralité des fichiers par FTP.

Il nécessite PHP5.5 ou plus.

## Télécharger

[ICI](https://get.spip.net/spip_loader.php)

## Question

Si vous vous posez l'une de ces questions :

- Utiliser spip_loader pour installer automatiquement SPIP
- Utiliser spip_loader pour effectuer une mise à jour
- Personnaliser les auteurs autorisés à effectuer une mise à jour
- Que faire en cas d’échec ?

La réponse se trouve sur la doc du loader sur le site de SPIP : [Utiliser spip_loader](https://www.spip.net/fr_article5705.html)

## Contribuer

Si vous souhaitez commiter sur ce projet, n'hésitez pas à proposer [une PR](https://git.spip.net/spip-contrib-outils/spip_loader/pulls)

Les traductions sont gérées [dans ce module](https://trad.spip.net/tradlang_module/tradloader)

### Attention

Depuis la version 5.2, spip_loader.php est [un fichier Phar](https://www.php.net/phar) qui nécessite une "compilation".

```bash
git clone https://git.spip.net/spip-contrib-outils/spip_loader.git /path/to/spip_loader
```

Pour produire le script :

La version du script est automatiquement mise à jour à partir du dernier tag du dépôt. Il est important que ce tag ne comporte pas de `v`en prefix.
[composer (v2)](https://getcomposer.org/) doit être présent sur la machine qui compile aussi.

```bash
cd /path/to/spip_loader
git pull
composer install
git tag x.y.x
php -d phar.readonly=0 ./compile
git push --tags
```

Exposer sur le web le contenu du dossier `/path/to/spip_loader/public_html`, site "statique".
