<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/tradloader?lang_cible=en
// ** ne pas modifier le fichier **

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_suivant' => 'Install >>',
	'bouton_suivant_maj' => 'Start the update >>',

	// C
	'ce_repertoire' => 'of this directory',

	// D
	'donnees_incorrectes' => '<h4>Incorrect data. Please try
again or use the manual installation procedure instead.</h4>
  <p>Error: @erreur@</p>',
	'du_repertoire' => 'of the directory',

	// E
	'echec_chargement' => '<h4>The download has failed. Please
try again or use the manual installation procedure instead.</h4>',
	'echec_php' => 'Your version of PHP @php1@ is not compatible with this version of SPIP that requires at least PHP @php2@.',

	// S
	'spip_loader_maj' => 'Version @version@ of spip_loader.php is available.',

	// T
	'texte_intro' => '<p>This program will download the files of @paquet@ into @dest@.</p>',
	'texte_preliminaire' => '<br /><h2>Preliminary step: <b>Set the access permissions</b></h2>
 <p><b>It is not possible to write to the
current directory.</b></p>
 <p>To change the permissions of the directory in
to which you are installing @paquet@, use your FTP client.<br /> The procedure is explained in detail in the Installation Guide. Choose between:</p>
 <ul>
 <li><b>If you have an FTP client with a graphical interface</b>, set the permissions
of the directory to make it open for everyone to write to it.</li>
 <li><b>If you have an FTP client with a text interface</b>, change the permissions of the directory to the value @chmod@.</li>
 <li><b>If you are using a Telnet connection</b>,
execute the command <i>chmod @chmod@ current_directory</i>.</li>
 </ul>
<p>Once this has been done, please <b><a href=\'@href@\'>reload this page</a></b>
 to start to upload and install SPIP.</p>
 <p>If you continue to receive this error notification, you will need to use the manual installation method
 (uploading the SPIP files by FTP) instead.</p>',
	'titre' => 'Download @paquet@',
	'titre_maj' => 'Update of @paquet@',
	'titre_version_courante' => 'Currently installed version: ',
	'titre_version_future' => 'Installation of version: '
);
