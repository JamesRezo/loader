<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/tradloader?lang_cible=es
// ** ne pas modifier le fichier **

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_suivant' => 'Empezar la instalación >>',
	'bouton_suivant_maj' => 'Lanzar la actualización >>',

	// C
	'ce_repertoire' => 'de esta carpeta',

	// D
	'donnees_incorrectes' => '<h4>Datos incorrectos. Vuelve a probar, o utiliza la instalación manual.</h4>
  <p>Erreur produite: @erreur@</p>',
	'du_repertoire' => 'de la carpeta',

	// E
	'echec_chargement' => '<h4>La descarga falló. Vuelve a probar, o utiliza la instalación manual.</h4>',
	'echec_php' => 'Tu versión de PHP @php1@ no es compatible con esta versión de SPIP que requiere al menos PHP @php2@.',

	// S
	'spip_loader_maj' => 'La versión @version@ de spip_loader.php está disponible.',

	// T
	'texte_intro' => '<p>El programa va a descargar los archivos de @paquet@ dentro de @dest@.</p>',
	'texte_preliminaire' => '<br /><h2>Preliminar: <b>Ajustar los derechos de acceso</b></h2>
<p><b>La carpeta actual no está accesible en modo escritura.</b></p>
<p>Para resolverlo, utiliza tu cliente FTP y ajusta los permisos de acceso
 a esta carpeta (carpeta de instalación de @paquet@).<br />
El proceso está explicado con detalle en la guía de instalación. Según el caso:</p>
<ul>
<li><b>Si tienes un cliente FTP gráfico</b>, ajusta las propiedades de la carpeta actual para que esté accesible en escritura para todos.</li>
<li><b>Si tu cliente FTP funciona en modo texto</b>, cambia el modo de la carpeta al valor @chmod@.</li>
<li><b>Si tienes un acceso ssh o Telnet</b>, ejecuta un <i>chmod @chmod@ carpeta_actual</i>./li>
</ul>
<p>Una vez efectuado este cambio, podrás <b><a href=\'@href@\'>volver a cargar esta página</a></b> para empezar la descarga y luego la instalación.</p>
<p>Si el error persiste, deberás pasar por el procedimiento de instalación clásico (subir todos los archivos por FTP).</p>',
	'titre' => 'Descarga de @paquet@',
	'titre_maj' => 'Actualización de @paquet@',
	'titre_version_courante' => 'Versión actualmente instalada: ',
	'titre_version_future' => 'Instalación de la versión: '
);
