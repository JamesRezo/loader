<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/tradloader?lang_cible=pt_br
// ** ne pas modifier le fichier **

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_suivant' => 'Iniciar a instalação &gt;&gt;',
	'bouton_suivant_maj' => 'Iniciar a atualização >>',

	// C
	'ce_repertoire' => 'deste diretório',

	// D
	'donnees_incorrectes' => '<h4>Dados incorretos. Tente novamente, ou use a instalação manual.</h4>
<p>Erro: @erreur@</p>',
	'du_repertoire' => 'do diretório',

	// E
	'echec_chargement' => '<h4>A transferência falhou. Tente novamente, ou use a instalação manual.</h4>',
	'echec_php' => 'A sua versão de PHP @php1@ não é compatível com esta versão do SPIP, que requer pelo menos o PHP @php2@.',

	// S
	'spip_loader_maj' => 'A versão @version@ do spip_loader.php está disponível.',

	// T
	'texte_intro' => '<p>O programa vai transferir os arquivos do @paquet@ para o @dest@.</p>',
	'texte_preliminaire' => '<br /><h2>Preliminar: <b>Ajustar os direitos de acesso</b></h2>
<p><b>O diretório atual não está acessível para escrita.</b></p>
<p>Para solucionar o problema, use o seu programa de FTP para ajustar os direitos de acesso a este diretório (diretório de instalação do @paquet@).<br />
O procedimento é explicado em detalhe no guia de instalação. Opções:</p>
<ul>
<li><b>Se você possui um programa de FTP gráfico</b>, ajuste as propriedades do diretório atual para que ele seja acessível para escrita para todos.</li>
<li><b>Se o seu programa de FTP é em modo caracter</b>, altere o modo do diretório para o valor @chmod@.</li>
<li><b>Se você tem um acesso Telnet</b>, faça um <i>chmod @chmod@ diretorio_atual</i>.</li>
</ul>
<p>Após fazer estes ajustes, você poderá <b><a href=\'@href@\'>recarregar esta página</a></b> para iniciar a transeferência e posterior instalação.</p>
<p>Se o erro persistir, você deverá optar pela instalação manual clássica
(transferência de todos os arquivos por FTP).</p>',
	'titre' => 'Transferência do @paquet@',
	'titre_maj' => 'Atualização de @paquet@',
	'titre_version_courante' => 'Versão instalada atualmante: ',
	'titre_version_future' => 'Instalação da versão: '
);
