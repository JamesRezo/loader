<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/tradloader?lang_cible=sk
// ** ne pas modifier le fichier **

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_suivant' => 'Spustiť inštaláciu >>',
	'bouton_suivant_maj' => 'Spustiť aktualizáciu >>',

	// C
	'ce_repertoire' => 'z tohto úložiska',

	// D
	'donnees_incorrectes' => '<h4>Nesprávne údaje. Prosím,
skúste to znova alebo namiesto toho využite manuálnu inštaláciu.</h4>
  <p>Chyba: @erreur@</p>',
	'du_repertoire' => 'úložiska',

	// E
	'echec_chargement' => '<h4>Sťahovanie sa nepodarilo. Prosím,
skúste to znova alebo namiesto toho využite manuálnu inštaláciu.</h4>',
	'echec_php' => 'Verzia vášho PHP @php1@ nie je kompatibilná s touto verziou SPIPu, ktorá si vyžaduje aspoň PHP @php2@.',

	// S
	'spip_loader_maj' => 'K dispozícii je spip_loader.php @version@.',

	// T
	'texte_intro' => '<b>Program stiahne súbory zásuvného modulu @paquet@ do @dest@.</p>',
	'texte_preliminaire' => '<br /><h2>Predpríprava: <b>Nastavenie prístupových povolení</b></h2>
<p><b>Do aktuálneho priečinka sa
nedá zapisovať.</b></p
<p>Na zmenu povolení priečinka,
v ktorom inštalujete @paquet@, použite svojho FTP klienta.<br /> Postup je podrobne opísaný v inštalačnej príručke. Vyberte si:</p>
<ul>
<li><b>Ak máte FTP klienta s grafickým rozhraním,</b> nastavte povolenia
priečinka, aby sa otvoril pre každého, kto doň chce zapisovať.</li>
<li><b>Ak máte FTP klienta s textovým rozhraním,</b> zmeňte povolenia priečinka na hodnotu @chmod@.</li>
<li><b>Ak využívate prístup cez Telnet,</b>
vykonajte príkaz <i>chmod @chmod@ current_directory.</i></li>
</ul>
<p>Keď to urobíte, prosím <b><a href=\'@href@\'>znova obnovte túto stránku,</a></b>
 aby ste mohli začať so sťahovaním a inštaláciou SPIPu.</p>
<p>Ak sa vám stále zobrazuje toto hlásenie o chybe, inštaláciu budete musieť vykonať manuálne (stiahnuť súbory SPIPu cez FTP).</p>',
	'titre' => 'Stiahnuť @paquet@',
	'titre_maj' => '@paquet@ bol aktualizovaný',
	'titre_version_courante' => 'Aktuálne nainštalovaná verzia: ',
	'titre_version_future' => 'Inštalácia verzie: '
);
