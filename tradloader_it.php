<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/tradloader?lang_cible=it
// ** ne pas modifier le fichier **

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_suivant' => 'Inizia l’installazione >>',
	'bouton_suivant_maj' => 'Avviare l’aggiornamento >>',

	// C
	'ce_repertoire' => 'di questa cartella',

	// D
	'donnees_incorrectes' => '<h4>Dati corrotti. Riprova o procedi all’installazione manuale.</h4>',
	'du_repertoire' => 'della cartella',

	// E
	'echec_chargement' => '<h4>Il caricamento è fallito. Riprova, o utilizza l’installazione manuale.</h4>',
	'echec_php' => 'La vostra versione di PHP @php1@ non è compatibile con questa versione di SPIP. È necessario avere almeno PHP @php2@.',

	// S
	'spip_loader_maj' => 'La versione @version@ di spip_loader.php è ora disponibile.',

	// T
	'texte_intro' => '<p><b>Benvenuto nella procedura d’installazione automatica di @paquet@.</b></p>
  <p>Il sistema ha verificato i permessi di scrittura per la cartella corrente.</p>
<p>Il programma procederà ora al download dei dati @paquet@ all’interno @dest@.</p>
  <p>Clicca sul tasto per continuare.</p>',
	'texte_preliminaire' => '<br /><h2>Configurazione preliminare: 
  <b>Regolare i permessi di scrittura</b></h2>
  <p><b>La cartella corrente non è accessibile in scrittura.</b></p>
  <p>Modificare i permessi della cartella corrente (la cartella in cui verrà installato @paquet@) utilizzando un client FTP.<br />
  La procedura è spiegata in dettaglio all’interno della guida di installazione. Alcuni consigli:</p>
  <ul>
  <li><b>Se hai un client FTP con interfaccia grafica</b>, modifica le proprietà della cartella corrente
  in modo da essere accessibile in scrittura da tutti.</li>
  <li><b>Se hai un client FTP in modalità testo</b>, modifica i permessi della cartella corrente impostandoli a @chmod@.</li>
  <li><b>Se utilizzi un accesso Telnet</b>, esegui il comando <i>chmod @chmod@ nome_cartella</i>.</li>
  </ul>
  <p>Una volta effettuata questa modifica, devi <b><a href=\'@href@\'>ricaricare questa pagina</a></b>
  per far partire il download e l’installazione.</p>
  <p>Se l’errore persiste, sarà necessario passare alla procedura di installazione classica
  (caricamento di tutti i file di SPIP via FTP).</p>',
	'titre' => 'Download di @paquet@',
	'titre_maj' => 'Aggiornamento di @paquet@',
	'titre_version_courante' => 'Ultima versione installata : ',
	'titre_version_future' => 'Installazione della versione : '
);
