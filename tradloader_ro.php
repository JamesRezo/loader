<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/tradloader?lang_cible=ro
// ** ne pas modifier le fichier **

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_suivant' => 'Începeţi instalarea >>',

	// C
	'ce_repertoire' => 'din acest director',

	// D
	'donnees_incorrectes' => '<h4>Date incorecte. Vă rugăm să încercaţi din nou sau să folosiţi instalarea manuală.</h4>
  <p>Eroare: @erreur@</p>',
	'du_repertoire' => 'din directorul',

	// E
	'echec_chargement' => '<h4>Încărcarea nu a fost reuşită. Vă rugăm să încercaţi din nou sau să folosiţi instalarea manuală.</h4>',

	// T
	'texte_intro' => '<p><b>Bine aţi venit în procedura de instalare automatică a lui @paquet@.</b></p>
  <p>Sistemul va verifica mai întâi drepturile de acces la directorul curent,
  şi după aceea va lansa descărcarea @paquet@ în interiorul @dest@.</p>
  <p>Vă rugam să apăsaţi pe butonul următor pentru a continua.</p>', # MODIF
	'texte_preliminaire' => '<br /><h2>Preliminar : <b>Reglaţi drepturile de acces</b></h2>
<p><b>Directorul curent nu este accesibil pentru scriere.</b></p>
<p>Pentru a corija aceasta, utilizaţi programul dumneavoastră preferat de FTP pentru a da drepturile de acces necesare în acest director (directorul de instalare @paquet@).<br />
Procedura este explicata în delaiu în ghidul de instalare. Aveţi la alegere :<br />
<ul>
<li><b>Dacă aveţi un client FTP grafic</b>, reglaţi proprietăţile directorului curent în aşa fel încât acesta să fie disponibil pentru scriere pentru toată lumea.</li>
<li><b>Dacă clientul FTP este în mod text</b>, schimbaţi modul de acces la directorul curent la valoarea @chmod@.</li>
<li><b>Dacă aveţi un acces de tip shell</b>, executaţi <i>chmod @chmod@ director_curent</i>.</li>
</ul>
<p>O dată terminată această etapă, puteţi <b><a href=\'@href@\'>reîncărca această pagină</a></b> pentru a pèutea începe descărcarea şi după aceea instalarea.</p>
<p>Dacă erorile persistă, va trebuie să folosiţi procedura de instalarea clasică (descărcarea tuturor fişierelor prin FTP).</p>',
	'titre' => 'Descărcarea lui @paquet@'
);
